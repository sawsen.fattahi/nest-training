import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from 'src/database/database.module';
import { ProjectsModule } from 'src/projects/projects.module';
import { ProjectRatingService } from './project-rating.service';

@Module({
  imports: [ConfigModule, ProjectsModule],
  providers: [ProjectRatingService],
})
export class ProjectRatingModule {}
