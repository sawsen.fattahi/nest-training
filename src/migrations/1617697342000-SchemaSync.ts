import {MigrationInterface, QueryRunner} from "typeorm";

export class SchemaSync1617697342000 implements MigrationInterface {
    name = 'SchemaSync1617697342000'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project" ADD "demo_link" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "demo_link"`);
    }

}
