import { IsString } from 'class-validator';
export class CreateProjectDto {
  @IsString()
  readonly name: string;

  @IsString({ each: true })
  readonly members: string[];

  @IsString()
  readonly git: string;
}
