import {
  Inject,
  Injectable,
  NotFoundException,
  Request,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { Connection, Repository } from 'typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Profile } from './entities/profile.entity';
import { Project } from './entities/project.entity';
import { Event } from '../events/entities/event.entity';
import { PROJECTS_TYPE } from './projects.constatnts';
import { ConfigService, ConfigType } from '@nestjs/config';
import projectsConfig from './config/projects.config';


@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    @InjectRepository(Profile)
    private readonly profileRepository: Repository<Profile>,
    private readonly connection: Connection,
    @Inject(PROJECTS_TYPE) projectsType: string[],
    @Inject(projectsConfig.KEY)
    private readonly projectsConfiguration: ConfigType<typeof projectsConfig>,
  ) {
    
    console.log('=======+++++++++++++++++++++++++++++++++', projectsConfiguration.foo);
  }

  findAll(paginationQuery: PaginationQueryDto) {
    const { limit, offset } = paginationQuery;
    return this.projectRepository.find({
      relations: ['members'],
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const project = await this.projectRepository.findOne(id);
    if (!project) {
      throw new NotFoundException(`Project with ${id} not found`);
    }
    return project;
  }

  async create(createProject: CreateProjectDto) {
    const members = await Promise.all(
      createProject.members.map((email) => this.preloadProfile(email)),
    );
    const project = this.projectRepository.create({
      ...createProject,
      members,
    });
    return this.projectRepository.save(project);
  }

  async update(id: string, updateProject: UpdateProjectDto) {
    const members =
      updateProject.members &&
      (await Promise.all(
        updateProject.members.map((email) => this.preloadProfile(email)),
      ));
    const existingProject = await this.projectRepository.preload({
      id: +id,
      ...updateProject,
      members,
    });
    if (!existingProject) {
      throw new NotFoundException(`project with ID: ${id} not found`);
    }
    this.recommendProject(existingProject);
    return this.projectRepository.save(existingProject);
  }

  async remove(id: string) {
    const project = await this.findOne(id);
    return this.projectRepository.remove(project);
  }

  async recommendProject(project: Project) {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      project.recommendation++;
      const recommendEvent = new Event();
      recommendEvent.name = 'recommend_project';
      recommendEvent.type = 'project';
      recommendEvent.payload = { projectId: project.id };

      await queryRunner.manager.save(project);
      await queryRunner.manager.save(recommendEvent);

      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release;
    }
  }

  private async preloadProfile(email: string): Promise<Profile> {
    const existingProfile = await this.profileRepository.findOne({
      email,
    });
    if (existingProfile) {
      return existingProfile;
    }
    return this.profileRepository.create({
      email,
      name: 'ananymous',
      phone: 111111111,
    });
  }
}
