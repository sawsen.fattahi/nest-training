import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Patch,
  Delete,
  Query,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectsService } from './projects.service';

@UsePipes(ValidationPipe)
@Controller('projects')
export class ProjectsController {
  constructor(private readonly projectsServices: ProjectsService) {
    console.log('controller is intiated');
  }
  @Get()
  findAll(@Query() paginationQuery: PaginationQueryDto) {
    return this.projectsServices.findAll(paginationQuery);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.projectsServices.findOne('' + id);
  }

  @Post()
  creat(@Body() createProject: CreateProjectDto) {
    return this.projectsServices.create(createProject);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body(ValidationPipe) updateProject: UpdateProjectDto) {
    return this.projectsServices.update(id, updateProject);
  }
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectsServices.remove(id);
  }
}
