import { registerAs } from '@nestjs/config';

export default registerAs('projects', () => ({
  foo: 'bar',
}));
