import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Profile } from './profile.entity';

@Entity()
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  demo_link: string;

  @Column({ default: 0 })
  recommendation: number;

  @JoinTable()
  @ManyToMany(() => Profile, (profile) => profile.projects, {
    cascade: true,
  })
  members: Profile[];

  @Column()
  git: string;
}
