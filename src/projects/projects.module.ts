import { Injectable, Module} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { Profile } from './entities/profile.entity';
import { Project } from './entities/project.entity';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { Event } from '../events/entities/event.entity';
import { PROJECTS_TYPE } from './projects.constatnts';
import { ConfigModule } from '@nestjs/config';
import projectsConfig from './config/projects.config';

class MockProjectsServices {}
class ConfigServiceAPP {}
class DevelopmentConfigService {}
class ProductionConfigService {}
@Injectable()
@Module({
  imports: [TypeOrmModule.forFeature([Project, Profile, Event]), ConfigModule.forFeature(projectsConfig)],
  controllers: [ProjectsController],
  providers: [
    {
      provide: ProjectsService,
      useClass: ProjectsService,
      useValue: new MockProjectsServices(),
    },
    {
      provide: ConfigServiceAPP,
      useClass:
        process.env.NODE_ENV === 'development'
          ? DevelopmentConfigService
          : ProductionConfigService,
    },
    {
      provide: PROJECTS_TYPE,
      useFactory: async (connection: Connection): Promise<string[]> => {
        const projectsTypes = await Promise.resolve(['API', 'Libriry']);
        console.log('[!] async factory');
        return projectsTypes;
      },
    },
  ],
  exports: [ProjectsService],
})
export class ProjectsModule {}
